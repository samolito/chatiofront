import React from 'react'

const PrivateChat= (props)=>{
    
    return(
        <div className="chatPage">
        <div className="chatSection">
          <div className="cardHeader">Chatroom Name</div>
         
          <div className="chatContent">
              <div  className="message">
                <span >
                  Ruth:
                </span>{" "}
                Hi How are you
              </div>
                        <div  className="message">
                            <span >
                            Ellen :
                            </span>{" "}
                            I'm good and you?
                        </div>
          </div>

          <div className="chatroomActions">
            <div>
              <input
                type="text"
                name="message"
                placeholder="Type your message here ..."
              />
            </div>
            <div>
              <button className="join" >Send </button>
            </div>
          </div>
        </div>
      </div>
    )
}


export default PrivateChat;