// import React   from 'react'
 import { Link } from 'react-router-dom/cjs/react-router-dom.min';
// import axios from 'axios'
// const DashboardPage= (props)=>{

// const chatroomNameRef=React.createRef();   
// const [chatrooms, setChatrooms] = React.useState([]);
// const ENDPOINT='http://localhost:4400/chatroom'
//     // const onCreateChatrroom=()=>{
//     //     const chatroomName=chatroomNameRef.current.value;
//     //     axios.post(ENDPOINT+'/create', {   
//     //         name:chatroomName
//     //    })
//     //    .then(function (response) {
//     //      alert(response.data.message);
//     //     // console.log(response.data.message)
//     //    })
//     //    .catch(function (err) {
//     //     alert( err.response.data.message);
//     //     setTimeout(getChatrooms,3000)
//     //    });
//     //  }
//      //get all the chatroom
//      const getChatrooms=()=>{
//         axios.get(ENDPOINT+'/getchatrooms',
//         {
//         //     headers:{
//         //     Authorization:localStorage.get('token'),
//         // },
//     })
//        .then( (response)=> {
//          setChatrooms(response.data.chatrooms.name)
//         console.log(response.data.)
//         //console.log(chatrooms.map((chat)=>{}))
//        })
//        .catch((err)=> {
//         //setTimeout(getChatrooms,3000)
//        });
//      }
//      React.useEffect(()=>{
//          getChatrooms();
//      },[])
//     return(
//         <div className="card">
//             <div className="cardHeader">Dashboard</div>
//                 <div className="cardBody">
//                     <div className="inputGroup">
//                         <label htmlFor="email">Chatroom Name</label>
//                         <input
//                         type="text"
//                         name="chatroomName"
//                         id="chatroomName"
//                         placeholder="Enter your chatroom Name "
//                         ref={chatroomNameRef}
//                         />
//                     </div>
//                     <button >Create Chatroom</button>
                   
//                     <div className="chatrooms">
                       
                         
//                             <div className="chatroom">
//                             <div>Haitian Fresh</div> <div className="join">Join</div>
//                             </div> 
//                     </div>
//                 </div>
//         </div>
//     );
// }

// export default DashboardPage;



import React from 'react'
import axios from 'axios'
const DashboardPage= (props)=>{

const chatroomNameRef=React.createRef();   
const [chatrooms, setChatrooms] = React.useState([]);

const ENDPOINT='http://localhost:4400/chatroom'
    const onCreateChatroom=()=>{
        const chatroomName=chatroomNameRef.current.value;
        axios.post(ENDPOINT+'/create', {   
            name:chatroomName
       })
       .then(function (response) {
     alert(response.data.message);
       
       })
       .catch(function (err) {
        alert( err.response.data.message);
       });
     }
React.useEffect(()=>{     
     //get all the chatroom
     const getChatrooms=()=>{
        axios.get(ENDPOINT+'/getchatrooms',
        {
        //     headers:{
        //     Authorization:localStorage.get('token'),
        // },
    })
       .then( (response)=> setChatrooms(response.data))
       .catch((err)=> {
            setTimeout(getChatrooms,3000)
       });
     }
     
         getChatrooms();
    // eslint-disable-next-line
     },[]);
     let chatroomsToRender;
     if (chatrooms) {
        chatroomsToRender = chatrooms.map(chatroom=> {
         return <div key={chatroom.id}>{chatroom.title}</div>;
       });
     } else {
        chatroomsToRender = "Loading...";
     }
    return(
        <div className="card">
            <div className="cardHeader">Dashboard</div>
                <div className="cardBody">
                    <div className="inputGroup">
                        <label htmlFor="email">Chatroom Name</label>
                        <input
                        type="text"
                        name="chatroomName"
                        id="chatroomName"
                        placeholder ="Enter your chatroom Name "
                        ref={chatroomNameRef}
                        />
                    </div>
                    <Link to={"/chatroom/"}>  <button onClick={onCreateChatroom} >Create Chatroom</button></Link>
                    <div className="chatrooms">
                        {chatroomsToRender}
                    </div>
                </div>
        </div>
    );
}

export default DashboardPage;
