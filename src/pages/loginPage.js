import React from 'react'
import axios from 'axios'
const LoginPage= (props)=>{
    const emailRef=React.createRef();
    const passwordRef=React.createRef();

    const onLogin=()=>{
       const email=emailRef.current.value;
       const password=passwordRef.current.value;
       axios.post('http://localhost:4400/api/user/login', {
        email: email,
        password:password,
      })
      .then(function (response) {
        //alert(response.data.message);
        console.log(response.data)
        localStorage.setItem('token',response.data.token)
        props.history.push('/chatroom')
      })
      .catch(function (err) {
       alert( err.response.data.message)
      });
       console.log(email,password)
    }
        
    
    const gotoRegister=()=>{
        props.history.push('/register')
    }
    return(
        <div className="card">
            <div className="cardHeader">Login</div>
                <div className="cardBody">
                    <div className="inputGroup">
                        <label htmlFor="email">Email</label>
                        <input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="abcd@mymail.com"
                        ref={emailRef}
                        />
                    </div>
                    <div className="inputGroup">
                        <label htmlFor="Password">Password</label>
                        <input
                        type="Password"
                        name="Password"
                        id="Password"
                        placeholder="Your Password"
                        ref={passwordRef}
                        />
                    </div>
                    <div>
                        <button onClick={onLogin}>Login</button>
                        <p></p>
                        <button onClick={gotoRegister}>Register</button>
                    </div>
                    <div></div>
                    
                </div>
        </div>
    );
}

export default LoginPage;