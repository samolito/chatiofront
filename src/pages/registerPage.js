import React from 'react'
import axios from 'axios'
const RegisterPage= (props)=>{
    const usernameRef=React.createRef();
    const emailRef=React.createRef();
    const passwordRef=React.createRef();
    const confirmPasswordRef=React.createRef();
    const onRegister=()=>{
       const username=usernameRef.current.value;
       const email=emailRef.current.value;
       const password=passwordRef.current.value;
       const confirmPassword=confirmPasswordRef.current.value;
    if (password !==confirmPassword) {
       console.log("Password must be same")
    } 
        axios.post('http://localhost:4400/api/user/register', {
        username: username,
        email: email,
        password:password,
      })
      .then(function (response) {
          //localStorage.getItem(response.messsage)
        alert(response.data.message);
        props.history.push('/login')
      })
      .catch(function (error) {
       alert(error);
      });
       console.log(username,email,password,confirmPassword)
    }
    return(
        <div className="card">
            <div className="cardHeader">Register</div>
                <div className="cardBody">
                <div className="inputGroup">
                        <label htmlFor="username">Username</label>
                        <input
                        type="text"
                        name="username"
                        id="username"
                        placeholder="Enter your username"
                        ref={usernameRef}
                        />
                    </div>
                    <div className="inputGroup">
                        <label htmlFor="email">Email</label>
                        <input
                        type="email"
                        name="email"
                        id="email"
                        placeholder="abcd@mymail.com"
                        ref={emailRef}
                        />
                    </div>
                    <div className="inputGroup">
                        <label htmlFor="Password">Password</label>
                        <input
                        type="Password"
                        name="password"
                        id="password"
                        placeholder="Your Password"
                        ref={passwordRef}
                        />
                    </div>
                    <div className="inputGroup">
                        <label htmlFor="confirmPassword">Confirm Password</label>
                        <input
                        type="Password"
                        name="confirmPassword"
                        id="confirmPassword"
                        placeholder="Re-enter password"
                        ref={confirmPasswordRef}
                        />
                    </div>
                    <div>
                        <button onClick={onRegister}>Register</button>
                    </div>
                </div>
        </div>
    )
}

export default RegisterPage;