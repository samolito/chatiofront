import React from 'react'
import '../styles/chat.css'
import io from 'socket.io-client'
// import onlineIcon from '../icon/onlineIcon.png'
// import chatIcon from '../images/chatroom.png'
import ChatForm from './chatrooms/chatForm'
import ChatTitle from './chatrooms/chatTitle'
import ChatMessageList from './chatrooms/ChatMessageList'
import ConversationList from './chatrooms/ConversationList'
const ChatroomPage= (props)=>{
    //const chatroomId= match.params.id;

   // const [user,setUserId ]= React.useState("")
    const ENDPOINT="http://localhost:4400"
    const socket = io(ENDPOINT)
//const 
const chatroomId =props.match.params.chatId;
  const [messages, setMessages] = React.useState([]);
  const messageRef = React.useRef();
  const [userId, setUserId] = React.useState("");

  const sendMessage = () => {
    if (socket) {
      socket.emit("chatroomMessage", {
        chatroomId,
        message: messageRef.current.value,
      });

      messageRef.current.value = "";
    }
  };

  React.useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      const payload = JSON.parse(atob(token.split(".")[1]));
      setUserId(payload.id);
    }
    if (socket) {
      socket.on("newMessage", (message) => {
        const newMessages = [...messages, message];
        setMessages(newMessages);
      });
    }
    //eslint-disable-next-line
  }, [messages]);

  React.useEffect(() => {
    if (socket) {
      socket.emit("joinRoom", {
        chatroomId,
      });
    }

    return () => {
      //Component Unmount
      if (socket) {
        socket.emit("leaveRoom", {
          chatroomId,
        });
      }
    };
    //eslint-disable-next-line
  }, []);

      return(
          <div className="chat-body"> 
                  <div className="chat-container"> 
                          <div className="search-container"> 
                              <input type="text" placeholder="Search ..."/>
                          </div>

                           <ConversationList/>

                          <div className="new-message-container"> </div>

                           <ChatTitle/>

                           <ChatMessageList/>

                          <ChatForm/>

                    </div>
              </div> 


)
}  


export default ChatroomPage;