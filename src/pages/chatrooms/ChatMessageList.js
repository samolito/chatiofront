import React from 'react'
import '../../styles/chat.css';
const ChatMessageList= (props)=>{
    
    return(
                <div className="chat-message-list"> 
                       <div className="message-row other-message">
                       <div className="message-text"> Good morning</div>
                       <div className="sender"> Ruth</div>
                       </div>

                       <div className="message-row your-message your-message">
                       <div className="message-text"> Hiii! How are you doing?</div>
                       <div className="sender"> Me</div>
                       </div>
                </div>
    );
}

export default ChatMessageList;