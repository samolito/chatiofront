import React from 'react';
import '../../styles/chat.css';
//import io from 'socket.io-client'
import onlineIcon from '../../icon/onlineIcon.png'

import axios from 'axios'

const ConversationList= (props)=>{
    const ENDPOINT='http://localhost:4400/chatroom'
    const [chatrooms, setChatrooms] = React.useState([{}]);  
    React.useEffect(()=>{     
        //get all the chatroom
        const getChatrooms=()=>{
           axios.get(ENDPOINT+'/getchatrooms',
           {
           //     headers:{
           //     Authorization:localStorage.get('token'),
           // },
       })
          .then( (response)=> {
           setChatrooms(response.data)
           //console.log(chatrooms)
          })
          .catch( (err)=> {
           //setTimeout(getChatrooms,3000)
          });
        }
       
        
             
            getChatrooms();
            // eslint-disable-next-line 
        },[chatrooms]);
    







                  return(
                        <div className="conversation-list"> 

                                    <div className="user-list">
                                      <h2>Users</h2> 
                                    <h4>
                                        <div className="activeItem">
                                        Herold <img alt="Online Icon" src={onlineIcon}/> 
                                        </div>
                                    </h4>
                                    <h4>
                                        <div className="activeItem">
                                           Manold <img alt="Online Icon" src={onlineIcon}/> 
                                        </div>
                                    </h4>
                                    <h4>
                                        <div className="activeItem">
                                            Samuel  <img alt="Online Icon" src={onlineIcon}/> 
                                        </div>
                                    </h4>
                                    
                                    </div>
                                    <div className="chatroom-list"> 
                                    <h2> Chatrooms </h2>
                                                <h4>
                                                    <div className="activeItem">
                                                        Haitian-Viet
                                                    </div>
                                                </h4>
                                    </div>
                                              
                          </div>
    );
}

export default ConversationList;