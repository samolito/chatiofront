import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios'
import { Input } from '@material-ui/core';
export default function CreateChatroomDialog() {
  const [open, setOpen] = React.useState(false);
  //const [chatrooms, setChatrooms] = React.useState([]);
  const [chatroom, setChatroom] = React.useState("");
  let chatroomNameRef=React.createRef()
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const ENDPOINT='http://localhost:4400/chatroom'
  const onCreateChatrroom=()=>{
     // let chatroomName='Capital002';
      setChatroom('chatroomName')
      alert(chatroom)
    
    //   axios.post(ENDPOINT+'/create', {   
    //       name:chatroomName
    //  })
    //  .then(function (response) {
    //    alert(response.data.message);
    //    handleClose();
    //   // console.log(response.data.message)
    //  })
    //  .catch(function (err) {
    //   alert( err.response.data.message+""+chatroomName);
    //  });
   };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Create Chatroom
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Chatroom Name</DialogTitle>
        <DialogContent>
          <Input
            margin="dense"
            id="chatroomName"
            placeholder ="Enter your chatroom Name "
            type="text"
            ref={chatroomNameRef}
          />
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={onCreateChatrroom} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
