import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import IndexPage from "./pages/indexPage";
import LoginPage from "./pages/loginPage";
import RegisterPage from "./pages/registerPage";
import ChatroomPage from "./pages/chatroomPage";
import DashboardPage from './pages/dashboardPage';
import PrivateChat from './pages/PrivateChat';
import io from 'socket.io-client'
function App() {
  const [socket, setSocket]=React.useState(null);

const setupSocket =()=>{
const token =localStorage.getItem('token');
if(token.length>0 && !socket){
  const newSocket=io('http://localhost:4400/chatroom',{
    query:{
      token:localStorage.getItem('token'),
    },
  });
  
  newSocket.on("disconnect",()=>{
    setSocket(null);
    setTimeout(setupSocket,3000);
    alert("Socket disconnected")

  });

  newSocket.on("connection",()=>{
    alert("Socket disconnected")
  });

  setSocket(newSocket);
}

}

React.useEffect(()=>{
  //setupSocket();
})
  return <BrowserRouter>
    <Switch>
    <Route path="/" component={IndexPage} exact/>
     
      <Route path="/login" 
      render={()=><LoginPage setupSocket={setupSocket}/>}
      component={LoginPage} exact/>
     
      <Route path="/register" 
      render={()=><RegisterPage setupSocket={setupSocket}/>}
      exact/>

      <Route path="/chatroom" 
       render={()=><ChatroomPage setupSocket={setupSocket}/>}
      exact/>

      <Route path="/dashboard" 
       render={()=><DashboardPage setupSocket={setupSocket}/>}
       exact/>
       
      <Route path="/chatroom" 
       render={()=><PrivateChat 
        setupSocket={setupSocket}/>}
       exact/>
    </Switch>
    </BrowserRouter>
}

export default App;
